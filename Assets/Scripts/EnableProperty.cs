﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
[System.AttributeUsage(System.AttributeTargets.Field, Inherited = true)]
public class EnableProperty : PropertyAttribute
{
    public enum Mode
    {
        Hide,
        Disable
    }

    public string EnableFieldName;
    public Mode EnableMode;

    public EnableProperty(string enableFieldName, Mode mode = Mode.Disable)
    {
        EnableFieldName = enableFieldName;
        this.EnableMode = mode;
    }
}

[CustomPropertyDrawer(typeof(EnableProperty))]
public class EnableProperty_PropertyDrawer : PropertyDrawer
{
    private bool IsEnable(SerializedProperty property)
    {
        EnableProperty obj = (EnableProperty)attribute;

        return property.serializedObject.FindProperty(obj.EnableFieldName).boolValue;
    }

    private void SetEnable(SerializedProperty property, bool enable)
    {
        EnableProperty obj = (EnableProperty)attribute;

        property.serializedObject.FindProperty(obj.EnableFieldName).boolValue = enable;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EnableProperty obj = (EnableProperty)attribute;

        float y = position.y;

        EditorGUI.BeginProperty(position, label, property);

        bool enable = IsEnable(property);

        if (enable || obj.EnableMode == EnableProperty.Mode.Disable)
        {
            position.height = base.GetPropertyHeight(property, label);

            EditorGUI.BeginDisabledGroup(!enable);

            EditorGUI.PropertyField(new Rect(position.x, y, position.width, position.height), property, true);
            EditorGUI.EndDisabledGroup();
        }

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        EnableProperty obj = (EnableProperty)attribute;
        bool enable = IsEnable(property);

        float totalHeight = 0;

        if (enable || obj.EnableMode == EnableProperty.Mode.Disable)
        {
            totalHeight += base.GetPropertyHeight(property, label);

            if (property.isExpanded)
            {
                SerializedProperty ite = property.Copy();
                SerializedProperty ite2 = property.Copy();

                ite2.NextVisible(false);

                ite.NextVisible(true);

                do
                {
                    if (!ite2.propertyPath.Equals(ite.propertyPath))
                    {
                        totalHeight += base.GetPropertyHeight(ite, label);

                        if (ite.isExpanded)
                        {
                            totalHeight += EditorGUIUtility.standardVerticalSpacing * 2;
                        }
                    }
                }
                while (ite.NextVisible(ite.isExpanded) && !ite2.propertyPath.Equals(ite.propertyPath));
            }

            totalHeight += EditorGUIUtility.standardVerticalSpacing * 2;
        }

        return totalHeight;
    }
}