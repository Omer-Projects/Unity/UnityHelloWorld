using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Property Classes
[System.Serializable]
public class ColorSceneChanger
{
    // Properties
    [Range(1, 16)]
    public int ColorsCount = 8;

    [Range(1, 200)]
    public int FrameIndexLastMin = 25;

    [Range(1, 200)]
    public int FrameIndexLastMax = 75;

    private int FrameIndex = 0;
    private int FrameIndexLast = 1;

    public Color[] colors = null;

    public void Update()
    {
        if (FrameIndex == 0)
        {
            FrameIndexLast = Random.Range(FrameIndexLastMin, FrameIndexLastMax + 1);

            Scene scene = SceneManager.GetActiveScene();

            GameObject[] gameObjects = scene.GetRootGameObjects();

            int colorIndex = 0;

            Color[] colors = new Color[ColorsCount];
            this.colors = colors;

            for (int i = 0; i < colors.Length; i++)
            {
                colors[i] = Random.ColorHSV();
            }

            foreach (int i in Utilities.CreatRandomIndexes(gameObjects.Length))
            {
                colorIndex = ChangeColor(gameObjects[i], colors, colorIndex);
            }
        }

        FrameIndex = (FrameIndex + 1) % FrameIndexLast;
    }

    int ChangeColor(GameObject gameObject, Color[] colors, int colorIndex)
    {
        MeshRenderer mesh;

        if (gameObject.TryGetComponent(out mesh))
        {
            foreach (int i in Utilities.CreatRandomIndexes(mesh.materials.Length))
            {
                mesh.materials[i].color = colors[colorIndex];
                colorIndex = (colorIndex + 1) % colors.Length;
            }
        }

        foreach (int i in Utilities.CreatRandomIndexes(gameObject.transform.childCount))
        {
            colorIndex = ChangeColor(gameObject.transform.GetChild(i).gameObject, colors, colorIndex);
        }

        return colorIndex;
    }
}

public class Manager : MonoBehaviour
{
    // Properties
    public bool EnableColorChanger = true;

    [EnableProperty("EnableColorChanger", EnableProperty.Mode.Hide)]
    public ColorSceneChanger ColorChanger = new ColorSceneChanger();

    // Events
    void Start()
    {
        
    }

    void Update()
    {
        if (EnableColorChanger)
		{
            ColorChanger.Update();
		}            
    }
}
