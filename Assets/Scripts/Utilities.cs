﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
static public class Utilities
{
    static public int[] CreatRandomIndexes(int count)
    {
        int[] indexes = new int[count];

        for (int i = 0; i < indexes.Length; i++)
        {
            indexes[i] = i;
        }

        for (int i = 0; i < indexes.Length; i++)
        {
            int j = Random.Range(0, indexes.Length);

            int temp = indexes[i];
            indexes[i] = indexes[j];
            indexes[j] = temp;
        }

        return indexes;
    }
}
