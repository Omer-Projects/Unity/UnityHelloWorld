﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace _LearnComp1
{
    [System.Serializable]
    [System.AttributeUsage(System.AttributeTargets.Field, Inherited = true)]
    public class ColoredProperty : PropertyAttribute
    {
        public enum Mode
        {
            Hide,
            Disable
        }

        public string ColorFieldName;

        public ColoredProperty(string colorFieldName)
        {
            ColorFieldName = colorFieldName;
        }
    }

    [CustomPropertyDrawer(typeof(ColoredProperty))]
    public class EnableProperty_PropertyDrawer : PropertyDrawer
    {
        private Color GetColor(SerializedProperty property)
        {
            ColoredProperty obj = (ColoredProperty)attribute;

            return property.serializedObject.FindProperty(obj.ColorFieldName).colorValue;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            Color color = GetColor(property);

            EditorGUI.DrawRect(position, color);

            EditorGUI.PropertyField(position, property, true);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float totalHeight = 0;

            totalHeight += base.GetPropertyHeight(property, label);

            if (property.isExpanded)
            {
                SerializedProperty ite = property.Copy();
                SerializedProperty ite2 = property.Copy();

                ite2.NextVisible(false);

                ite.NextVisible(true);

                do
                {
                    if (!ite2.propertyPath.Equals(ite.propertyPath))
                    {
                        totalHeight += base.GetPropertyHeight(ite, label);

                        if (ite.isExpanded)
                        {
                            totalHeight += EditorGUIUtility.standardVerticalSpacing * 2;
                        }
                    }
                }
                while (ite.NextVisible(ite.isExpanded) && !ite2.propertyPath.Equals(ite.propertyPath));
            }

            totalHeight += EditorGUIUtility.standardVerticalSpacing * 2;

            return totalHeight;
        }
    }
}