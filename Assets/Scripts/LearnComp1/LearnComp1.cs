using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class LearnComp1 : _LearnComp1.LearnComp { }

namespace _LearnComp1
{
    [System.Serializable]
    public class CustomColor
    {
        public Color Color = Color.white;
        public Color Color2 = Color.white;
        public Color Color3 = Color.white;
    }

    [System.Serializable]
    public class CustomVector
    {
        public Vector3 Vector = Vector3.zero;

        public CustomColor CustomColor;
    }

    [System.Serializable]
    public class EnableCustomColor
    {
        [ColoredProperty("BackColor")]
        public CustomColor CustomColor;
    }

    [System.Serializable]
    public class EnableCustomColor2
    {
        [EnableProperty("EnableCustomColor", EnableProperty.Mode.Hide)]
        public Color Color = Color.white;

        [EnableProperty("EnableCustomColor", EnableProperty.Mode.Hide)]
        public Color Color2 = Color.white;

        [EnableProperty("EnableCustomColor", EnableProperty.Mode.Hide)]
        public Color Color3 = Color.white;
    }

    public class LearnComp : MonoBehaviour
    {
        // Members
        public bool EnableCustomColor = true;
        public bool EnableCustomVector = true;
        public bool EnablePos = true;

        [EnableProperty("EnableCustomColor", EnableProperty.Mode.Disable)]
        public Color BackColor = Color.white;

        [EnableProperty("EnableCustomColor", EnableProperty.Mode.Hide)]
        public EnableCustomColor CustomColor;

        [ColoredProperty("BackColor")]
        public EnableCustomColor2 CustomColor2;

        [EnableProperty("EnableCustomVector", EnableProperty.Mode.Hide)]
        public CustomVector CustomVector;

        [EnableProperty("EnablePos", EnableProperty.Mode.Hide)]
        public Vector3 Pos = Vector3.zero;

        public bool EnableCustomVector2 = true;

        [EnableProperty("EnableCustomVector2")]
        public CustomColor CustomVector2;

        public CustomVector CustomVector3;

        // Events
        void Start()
        {

        }

        void Update()
        {

        }
    }
}